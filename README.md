iframeupload
===================================
iframeupload是通過iframe實現的無刷新上傳jQuery插件，簡單、易用、靈活。

作者
-----------------------------------
[喵了个咪](http://www.hdj.me)

版本
-----------------------------------
v1.3.3

調用
-----------------------------------
html:
```html
<img class="iframeupload" src="#" />
```
javascript：
```javascript
$('img.iframeupload').iframeUpload({
    action: 'upload.php',
    name: 'uploadfile',
    allowTypes: ['jpg', 'png', 'gif'],
    onChange: function(file, ext){
        // ...
    },
    onSubmit: function(file, ext){
        // ...
    },
    onComplete: function(file, response){
        // ...
    },
    onError: function(error){
        // ...
    }
});
```

參數
-----------------------------------
### action
類型：string  
說明：提交上傳地址，可通過html屬性action定義  
默認：upload.php  
例子：  
```html
<img class="iframeupload" action="/uploadImg.php" />
```

### name
類型：string  
說明：提交文件input名稱，可通過html屬性name定義  
默認：uploadfile  
例子：  
```html
<img class="iframeupload" name="uploadphoto" />
```

### multiple
類型：bool  
說明：是否使用批量上傳，name會自動識別轉為name[]格式，可通過html屬性multiple定義  
默認：false  
例子：
```html
<img class="iframeupload" multiple="1" />
```

### allowTypes
類型：array/string  
說明：允許文件類型，可通过html属性定义allowTypes  
默認：[]  
例子：
```html
<img class="iframeupload" allowTypes="jpg,gif,png" />
```

### data
類型：object  
說明：附加參數，可通過html屬性data-xx定義  
默認：{}  
例子：  
```html
<img class="iframeupload" data-id="1" data-other="other" />
```

### dataType
類型：string  
說明：返回數據類型，json或空  
默認：'json'  

### onChange
類型：function  
說明：文件被選擇後回調方法  
默認：function(file, ext){}  

### onSubmit
類型：function  
說明：文件被提交後回調方法  
默認：function(file, ext){}  

### onComplete
類型：function  
說明：文件上傳成功後回調方法  
默認：function(file, response){}  

### onError
類型：function  
說明：選擇非法類型後回調方法  
默認：function(error){}  

更新说明
-----------------------------------
### 1.3.3
增加 支持html属性定义参数  
增加 支持批量上传  
修正 allowTypes为空不能正常上传的问题  
优化 allowTypes参数支持string格式  
增加 支持同步发布  
### 1.3.2
增加 生成随机字符串  
优化 同一个页面多次调用逻辑  
优化 代码架构，每个el生成独立的form  
### 1.3.1
增加 addEvent方法，解决无法绑定iframe事件问题  